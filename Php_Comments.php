<?php

// Total 8 data types
   // Among them 4 data Types are Scalar Types
   //Boolean


     $myTestVar = true;
     var_dump($myTestVar);

     echo "<br>";

   //integer
    $myTestVar = 64;
    var_dump($myTestVar);

    echo "<br>";

    //float
    $myTestVar = 6.4;
    var_dump($myTestVar);

    echo "<br>";

    //Single Coated String
    $myTestVar = "Hello Single Quoted String";
    var_dump($myTestVar);

    echo "<br>";